#!/bin/bash

[[ ! -e /.dockerenv ]] && exit 0

set -euo pipefail

apt-get update -yqq
apt-get install git libzip-dev unzip -yqq

# Install composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php --install-dir=/usr/local/bin/ --filename=composer
php -r "unlink('composer-setup.php');"

docker-php-ext-install zip
