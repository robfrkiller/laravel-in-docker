## 問題一：
我們需要在Google Cloud Plantform 上建置API Server 並建置CI/CD ，開發語⾔要以Laravel 並搭配 Docker 為主。請模擬寫⼀篇建置⽂件。
內容請包含如下的說明：
## 1. 在 Laravel 專案中，你如何配置 GitLab CI/CD pipeline？請解釋主要的步驟和設定檔案。

在專案根目錄新增一個 `.gitlab-ci.yml`，並設置以下

- 運行的環境(image)
- 執行各階段工作(job)
    - 各工作需要執行的腳本(script)

## 2. 請解釋 Laravel 的⾃動化測試在 CI/CD 流程中的作⽤。你會使⽤哪些測試⼯具和⽅法。

用來確認產出的程式碼，預期效果與實際結果是否一致

Laravel 已整合以下測試工具

- phpunit: 可用於單元測試、整合測試、API 測試
- pint: 可用於測試程式碼 coding style 是否符合規範

## 3. 如何在 GitLab CI/CD pipeline 中使⽤ Docker？你認為使⽤ Docker 的好處是什麼？

於 job 中設置 `image` 指定 docker image，即可於從容器中執行腳本

好處是

- 隔離運行環境，確保每次執行時皆為獨立狀態
- 省資源，啟動快速
- 提高執行環境可攜性
- 跨平臺

## 4. 在 CI/CD 流程中，如何處理環境變數和機密資訊（如敏感 API ⾦鑰）的設定和保護？

`.gitlab-ci.yml` 可設定 `variables` 變數

或可至 Gitlab 專案中 settings → CI/CD → Variables 設置

機密資訊可 Visibility 設置為 Masked，確保不會外流至 job logs

## 5. 如何監視和追蹤 CI/CD pipeline 的執⾏狀態和錯誤？你使⽤過哪些⼯具或⽅法？

- 帳號預設會透過 Email 通知執行異常
- 各專案下的 Build 功能
- 專案 Settings → Webhooks 請求外部 API
- 專案 Settings → Integrations 整合各家通訊軟體

## 問題二：
Laravel ⽬前是公司核⼼在使⽤的技術，簡答以下⽂題：
## 1. Laravel 中的 Middleware 是什麼？是說明Middleware的功⽤。

中介層，可作為在實際執行業務流程(Controller)之前或之後，增加需要額外處理的事務

常用於身份驗證，確認使用者是否擁有權限執行 API

另外官方所提供的 Middleware

- \Illuminate\Foundation\Http\Middleware\ValidateCsrfToken::class => 解決 CSRF 問題
- \Illuminate\Foundation\Http\Middleware\TrimStrings::class => 將請求所有字串參數去除前後空白字元

## 2. 什麼是 Eloquent ORM？附上⼀組你寫過的Eloquent ORM來做範例，並說明此範例的功能。

將資料庫查詢語法，包裝後改成以物件導向方式從 Model(資料表) 查詢所需資料，可更簡易的執行 SQL 語句

```PHP
// 將 Query Builder 物件存為變數後，更易於將此查詢重用至他處。
$queryBuilder = \App\Models\User::query()
    // 不用拘束於原生 SQL 順序規則，如 order by 需要置於語法後段，在需要時就能直接寫入查詢語法。
    ->orderByDesc('id')
    ->take(3)
    ->select('id')
    // 若需判斷變數是否存在，來決定是否要 where 某些條件。
    ->when($params['name'] ?? null, function (Builder $builder, string $name) {
        $builder->orWhereAny(['account', 'name'], 'LIKE', $name);
    });
```

## 3. JWT 的應⽤流程。

可用於第三方系統身份授權認證

當使用者進入系統並完成身份驗證後

授權系統將以與第三方約定好的對稱式密鑰簽發出 JWT token 並提供給使用者

使用者即可進入第三方系統並提供 JWT 確認權限

第三方系統使用約定好的對稱式密鑰對 JWT token 驗證

若驗證通過且 JWT 仍在有效期間

則可確認使用者已取得身份授權
